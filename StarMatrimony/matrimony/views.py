from django.shortcuts import render,redirect
from django.http import JsonResponse,HttpResponse
from .models import UserProfile,Interested_list
from django.views.decorators.http import require_POST
from django.contrib.auth import logout
import hashlib
# Create your views here.


def index(request):
    return render(request,'matrimony/login.html',{'error' : ''})

def signin(request):
    return render(request,'matrimony/sign.html')

# returns JSON data
def check_username(request):
    if(request.is_ajax()):
        username = request.POST['regusername']
        
        # json data
        data = {
                'boolean' : UserProfile.objects.filter(username__exact = username).exists()
            }
        
        return JsonResponse(data)
    return HttpResponse("Some Error")

# returns JSON data
def check_phone(request):
    if(request.is_ajax()):
        phone = request.POST['phone']
            
        data = {
                'boolean' : UserProfile.objects.filter(phone__exact = phone).exists()
            }
        return JsonResponse(data)
    
# Inserts data into database
def register_me(request):
    if(request.is_ajax()):
        
        password = request.POST['regpassword']
        password = hashlib.md5(password.encode())
        password = password.hexdigest()
        obj = UserProfile(
                firstname = request.POST['firstname'],
                lastname = request.POST['lastname'],
                username = request.POST['regusername'],
                dob = request.POST['dob'],
                religion = request.POST['religion'],
                gender = request.POST['gender'],
                phone = request.POST['phone'],
                email = request.POST['email'],
                password = password
            )
        
        try:
            obj.save()
            boolean = True
        except Exception as e:
            print(e)
            boolean = False
        data = {
                'boolean' : boolean
            }
        return JsonResponse(data)
    
    
# @require_POST
def login(request):
    # not an ajax method
    username = ''
    if(request.method == 'POST'):
        password = request.POST['regpassword']
        password = hashlib.md5(password.encode())
        password = password.hexdigest()
        print(password)
        username = request.POST['username']
        obj = UserProfile.objects.filter(
                username__exact = request.POST['username'],
                password__exact = password
            )
        
        # password and username matches
        if(obj):
            obj = UserProfile.objects.filter(username__exact = request.POST['username'],
                                         password__exact = str(password))
            context = {'obj' : obj}
            request.session['username'] = username
#             print(request.session[username])
            print(request.session['username'])
            return render(request,'matrimony/profile.html',context)
        # if it does not match then
        else:
#             print("Yaha tak pohocha")
            
#             return HttpResponse("Username and password didn't match")
            return render(request,'matrimony/login.html',{'error' : 'Your username and password didn\'t match'})    
    elif('username' in request.session):
        obj = UserProfile.objects.get(username__exact = request.session['username'])
        context = {'obj':obj}

        return render(request,'matrimony/profile.html',context)
# Profile

def upload_photo(request):
    if(request.method == 'POST'):
        try:
            avatar = request.FILES['file']
            
            # GET OBJECT FROM PASSED USERNAME
            
            obj = UserProfile.objects.get(username__exact = request.session['username'])
            obj.avatar = avatar
            obj.save()
        except Exception as e:
            return HttpResponse("Please select image first")
        else:
            return render(request,'matrimony/profile.html',{'obj' : obj})
    elif(request.method == 'GET'):
        obj = UserProfile.objects.get(username__exact = request.session['username'])
        
    return render(request,'matrimony/profile.html',{'obj': obj})

# Personal information update
def update_personal_information(request):
    if(request.is_ajax()):
        expectations = request.POST['expectations']
        interests = request.POST['interests']
        global username
        obj = UserProfile.objects.get(username = request.session['username'])
        obj.expectations = expectations
        obj.interests = interests
        obj.qualification = request.POST['qualification']
        obj.working_at = request.POST['working_at']
        obj.marital_status = request.POST['marital_status']
        obj.save()
        
    data = {
        'is_updated' : True
    }
    
    return JsonResponse(data)

# Home page

def homepage(request):
    obj = UserProfile.objects.get(username__exact = request.session['username'])
    interested_list = Interested_list.objects.filter(interest_in = obj.username)
    interested_list = interested_list[::-1]
    return render(request,'matrimony/home.html',{'obj' : obj , 'interested_list' : interested_list })

def SearchFor(request):
            
    if(request.method=='POST'):
        # userobject 
        obj1 = UserProfile.objects.get(username = request.session['username'])
        
        # get all the requested users
        obj2 = UserProfile.objects.filter(username__icontains = request.POST['name'])
        
        # get all the opposite gender's data
        obj3 = UserProfile.objects.exclude(gender = obj1.gender)
        
        # perform intesection of both query set
        obj = obj2 & obj3
        
#         context = {'obj' : obj , 'obj1' : obj1}
        
        return render(request,'matrimony/search_category.html',{'obj' : obj})
    else:
        return HttpResponse("GET")
    
def search_category(request):
    if(request.POST):
        category = request.POST['category']
        print(category)
#         print(request.session['username'])
        obj = UserProfile.objects.get(username = request.session['username'])
        if(category == 'all'):
            obj1 = UserProfile.objects.all()
        else:
            obj1 = UserProfile.objects.filter(religion = category)
        obj2 = UserProfile.objects.exclude(gender = obj.gender)
        obj = obj1 and obj2
#         print(obj1)
#         print(request.session)
    return render(request,'matrimony/search_category.html',{'obj' : obj})



# render's selected profile
def special_profile(request,selected_username):
    # working with GET request only
#     if(request.method == 'POST'):
    obj = UserProfile.objects.get(username = request.session['username'])
    
    requested_for = UserProfile.objects.get(username = selected_username)
    
    # possibility 1
    data1 = Interested_list.objects.filter(interest_by = request.session['username'] , interest_in = selected_username).exists()
        
    # possibility 2
    data2 = Interested_list.objects.filter(interest_by = selected_username , interest_in = request.session['username']).exists()
    
    both_interested = data1 and data2
    print("hello world")
    print(both_interested)
    return render(request,'matrimony/special_profile.html',{'requested_for' : requested_for,'obj' : obj,
                                                            'both_interested' : both_interested,
                                                            })

#     return HttpResponse()
    
# trial 
def AddToList(request):
    if(request.is_ajax()):
        interest_by = request.POST['username']
        interest_in = request.POST['interested_in']
        
        obj = Interested_list.objects.filter(interest_by = interest_by,interest_in = interest_in).exists()
        if(not obj):
            print(f"{interest_by} is interested in {interest_in}")
            obj = Interested_list(interest_by = interest_by,
                                  interest_in = interest_in
                                  )
            obj.save()
            data = {
                    'is_updated' : True
                }
        else:
            data = {
                    'is_updated' : False
                }
        return JsonResponse(data)
    return HttpResponse("Out")

def remove_from_interested(request):
    if(request.method == 'POST'):
        obj = UserProfile.objects.get(username = request.session['username'])
    
        selected_username = request.POST['block']
        requested_for = UserProfile.objects.get(username = selected_username)
        
        # possibility 1
        data1 = Interested_list.objects.filter(interest_by = request.session['username'] , interest_in = selected_username).exists()
            
        # possibility 2
        data2 = Interested_list.objects.filter(interest_by = selected_username , interest_in = request.session['username']).exists()
        
        both_interested = data1 and data2
        
        if(both_interested):
            data1 = Interested_list.objects.filter(interest_by = request.session['username'] , interest_in = selected_username)
            data1.delete()
#         both_interested.save()

        return JsonResponse({'data' : True})
    
def logout_finally(request):
    if(request.method=='POST'):
#         print(dir(request.session))
#         request.session.pop('username')
        del request.session['username']
        request.session.modified = True
    return redirect('index')
