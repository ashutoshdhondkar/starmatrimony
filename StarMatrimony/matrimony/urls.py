from django.urls import path
from . import views
urlpatterns = [
    path('login/',views.index,name='index'),
    path('signin/',views.signin,name='signin'),
    path('validateusername/',views.check_username,name='check_username'),
    path('validatephone/',views.check_phone,name='check_phone'),
    path('register/',views.register_me,name='register_me'),
    path('profile/',views.login,name='login'),
    path('photo/',views.upload_photo,name='upload_photo'),
    path('update_personal_information/',views.update_personal_information
        ,name='update_personal_information'),
    path('homepage/',views.homepage,name='homepage'),
    path('justtry/',views.SearchFor,name='justtry'),
    path('view/<selected_username>',views.special_profile,name = 'special_profile'),
    path('add/',views.AddToList,name='add'),
    path('search_cat/',views.search_category,name = 'search_category'),
    path('logout/',views.logout_finally,name='logout'),
    path('remove_interested/',views.remove_from_interested,name="blockit"),
]