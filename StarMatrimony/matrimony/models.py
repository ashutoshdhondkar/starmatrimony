from django.db import models

# Create your models here.
class UserProfile(models.Model):
    
    firstname = models.CharField(max_length = 20)
    lastname = models.CharField(max_length = 20)
    username = models.CharField(max_length = 20)
    dob = models.CharField(max_length = 20)
    religion = models.CharField(max_length = 10)
    gender = models.CharField(max_length = 6)
    phone = models.CharField(max_length = 10)
    email = models.CharField(max_length = 50)
    password = models.CharField(max_length = 20)
    
    # profile options
    avatar = models.ImageField(upload_to = 'photos',default = 'photos/1234.png')
    expectations = models.TextField(blank = True)
    interests = models.TextField(blank = True)
    qualification = models.TextField(blank = True)
    working_at = models.TextField(blank = True)
    marital_status = models.CharField(max_length = 10,blank = True)
    
    
    def __str__(self):
        return self.username
    
class Interested_list(models.Model):
    
    interest_by = models.CharField(max_length = 20)
    interest_in = models.CharField(max_length = 20)
     
    def __str__(self):
        return f"<< {self.interest_by} | {self.interest_in} >>"
    
class MatchedCouples(models.Model):
    pass
    